// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAa-LWKVw-9_unzWSIzNeARM5tx8Qr4nl4',
    authDomain: 'firestore-bf4e5.firebaseapp.com',
    databaseURL: 'https://firestore-bf4e5.firebaseio.com',
    projectId: 'firestore-bf4e5',
    storageBucket: 'firestore-bf4e5.appspot.com',
    messagingSenderId: '348854879826'
  }
};
