import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

export interface Item { name: string; price: number; }

@Component({
  selector: 'app-root',
  template:
  `<p *ngFor="let item of items | async">
    {{ item.name }} is {{ item.price }}
  </p>`,
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  itemCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  constructor(private afs: AngularFirestore) {
    this.itemCollection = this.afs.collection<Item>('items');
    this.items = this.itemCollection.valueChanges();
  }
}
